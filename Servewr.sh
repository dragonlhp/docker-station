#!/bin/bash
ssh -o PasswordAuthentication=no    \
    -o StrictHostKeyChecking=no     \
    -o UserKnownHostsFile=/dev/null \
    -o TCPKeepAlive=yes             \
    -o ServerAliveInterval=30       \
    -p 22                   \
    -i ~/.ssh/id_rsa    \
    root@94.191.19.69 \
    -t 'cd /opt/docker; zsh --login'
